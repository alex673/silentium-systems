import React from "react"
export default function Header(props) {
  return <h1 id="slogan">{props.headerText}</h1>
}