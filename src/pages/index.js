import React from "react"
import styled from 'styled-components'
import Header from "../components/header"
import logo from "../images/logo.png"
import linkedin from "../images/linked-in.png"
import gscholar from "../images/gscholar.png"
import { css } from '@emotion/react'

export default () => (
	<div class="container-fluid">
		<div css={css`background: url(hero-img.jpg);`} id="main-bg">
			<div class="container header-section">
				<div class="row">
					<div class="col-lg">
						<a href="/"><img class="float-left" src={logo} id="logo"/></a>
						<a href="/" class="company-name float-left">Silentium Systems</a>                                                                                                                                         
					</div>
					<div class="col-lg">
						<ul class="nav justify-content-end" id="top-menu">
						<li class="nav-item">
							<a class="nav-link active" href="#">Expert Software</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">About</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Technologies</a>
						</li>
						<li>
							<button id="contact-us" type="button" class="btn btn-success">Contact us</button>
						</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="container">
				<Header headerText="Forensic Expert Systems built with AI, Neural Networks, and Deep Learning"/>
			</div>
			<div class="container footer">
				<div class="row">
					<div class="col-lg-6">
						<ul class="social-icons">
							<li><a href="https://www.linkedin.com/company/silentium-systems/"><img src={linkedin}/>Linked in</a></li>
							<li><a href="https://scholar.google.com/citations?user=RHo2Q5oAAAAJ&hl=en"><img src={gscholar}/>Google scholar</a></li>
						</ul>
					</div>
					<div class="col-lg-6">
						<p class="copyright">Copyright (c) 2020</p>
					</div>
				</div>
			</div>
		</div>
	</div>
)
